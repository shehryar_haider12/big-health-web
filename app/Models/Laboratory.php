<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Auth;
use App\Models\TestCategory;
use App\Models\Banner;

class Laboratory extends Model
{
    use SoftDeletes;

    protected $table = 'laboratories';
    protected $fillable = ['image','name','address','created_by','updated_by','status'];

    /**
     * Get all of the comments for the Laboratory
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function testLaboratory()
    {
        return $this->hasMany(TestCategory::class, 'foreign_id', 'id')->where('type','laboratory');
    }

    /**
     * Get all of the banners for the Laboratory
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function banners()
    {
        return $this->hasMany(Banner::class, 'foreign_id', 'id')->where('type','laboratory');
    }

    

    public static function boot()
    {
       parent::boot();
       static::creating(function($model)
       {
           $user = Auth::user();
           $model->created_by = $user->id;
           $model->updated_by = $user->id;
       });
       static::updating(function($model)
       {
           $user = Auth::user();
           $model->updated_by = $user->id;
       });
    }
}
