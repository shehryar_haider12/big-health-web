<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\User;

class Contact extends Model
{
    protected $table = 'contact_us';
    protected $fillable = ['user_id','name','email','message'];

     /**
     * Get the test associated with the Cart
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function user()
    {
        return $this->hasOne(User::class, 'id', 'user_id');
    }
}
