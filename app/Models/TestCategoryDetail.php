<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\LaboratoryTest;
use App\Models\TestCategory;
use App\Models\Category;

class TestCategoryDetail extends Model
{
    protected $table = 'test_categories_detail';
    protected $fillable = ['test_id','category_id'];

    /**
     * Get the test associated with the TestCategory
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function test()
    {
        return $this->hasOne(LaboratoryTest::class, 'id', 'test_id');
    }

    /**
     * Get the test associated with the TestCategory
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function testCategory()
    {
        return $this->hasOne(TestCategory::class, 'test_id', 'test_id');
    }

    /**
     * Get the test associated with the TestCategory
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function category()
    {
        return $this->hasOne(Category::class, 'id', 'category_id');
    }
}
