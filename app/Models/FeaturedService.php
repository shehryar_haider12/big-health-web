<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Auth;

class FeaturedService extends Model
{    
    protected $table = 'featured_services';
    protected $fillable = ['image','name','created_by','updated_by','status'];

    public static function boot()
    {
       parent::boot();
       static::creating(function($model)
       {
           $user = Auth::user();
           $model->created_by = $user->id;
           $model->updated_by = $user->id;
       });
       static::updating(function($model)
       {
           $user = Auth::user();
           $model->updated_by = $user->id;
       });
    }
}
