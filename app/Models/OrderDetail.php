<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\TestCategory;

class OrderDetail extends Model
{
    protected $table = 'order_details';
    protected $fillable = ['order_no','user_id','test_id','laboratory_id','checkout_id','amount'];

     /**
     * Get the test associated with the Cart
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function test_category()
    {
        return $this->hasOne(TestCategory::class, 'id', 'test_id');
    }
}
