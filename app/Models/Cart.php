<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\TestCategory;

class Cart extends Model
{
    protected $table = 'add_to_cart';
    protected $fillable = ['amount','user_id','quantity','test_id'];

     /**
     * Get the test associated with the Cart
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function test_category()
    {
        return $this->hasOne(TestCategory::class, 'id', 'test_id');
    }
}
