<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Laboratory;
use App\Models\LaboratoryTest;
use App\Models\TestCategory;
use App\Models\IncludeTest;

class TestCategory extends Model
{
    protected $table = 'test_category';
    protected $fillable = ['test_id','fee','recommend','report_in_days','foreign_id','type'];

    /**
     * Get the test associated with the TestCategory
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function test()
    {
        return $this->hasOne(LaboratoryTest::class, 'id', 'test_id');
    }

        /**
     * Get the test associated with the TestCategory
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function laboratory()
    {
        return $this->hasOne(Laboratory::class, 'id', 'foreign_id');
    }

    /**
     * Get all of the category for the TestCategory
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function category()
    {
        return $this->hasMany(TestCategory::class, 'test_id', 'test_id')->where('type','category');
    }

    /**
     * Get all of the banners for the Laboratory
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function includeTests()
    {
        return $this->hasMany(IncludeTest::class, 'lab_test_id', 'id');
    }

}
