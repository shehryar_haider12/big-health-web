<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\TestCategory;

class Checkout extends Model
{
    protected $table = 'checkout';
    protected $fillable = ['order_no','user_id','address','optional_address','phone','email','payment_method'];

     /**
     * Get the test associated with the Cart
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function test_category()
    {
        return $this->hasOne(TestCategory::class, 'id', 'test_id');
    }
}
