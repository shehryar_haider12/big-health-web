<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\LaboratoryTest;

class IncludeTest extends Model
{
    protected $table='include_tests';
    protected $fillable=[
      'lab_test_id',
      'test_id',
    ];

    /**
     * Get the test associated with the IncludeTest
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function test()
    {
        return $this->hasOne(LaboratoryTest::class, 'id', 'test_id');
    }
}
