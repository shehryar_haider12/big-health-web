<?php
use App\Models\Message;
use App\Models\Notification;
use App\Models\Transaction;
use App\Models\Membership;
use App\Models\LoginHistory;
use App\Models\Cart;
use App\Models\Slider;


 function result($data, $status, $message)
 {
    return response()->json([
        "data"      =>  $data,
        "code"      =>  $status,
        "message"   =>  $message,
        "image_url" =>  url('')."/uploads/",
    ], $status);
 }

function cart_count()
{
   // if(Auth::check())
   // {
      $id =auth()->user()->id ?? 0;
      return Cart::with('test')->where('user_id',$id)->count();
   // }
}

function sliders()
{
      return Slider::get();
}


/**
 * Get Specified Site Setting
 * @return array 
 */
//  function getSiteSetting($key)
//  {
//     $setting =  Setting::where('key',$key)->first();
//     return $setting->value;
//  }

//  /**
//  * Get Specified Site Setting
//  * @return array 
//  */
// function loginHistory($data)
// {
//    $data['datetime'] = now();
//    LoginHistory::create($data);  
// }