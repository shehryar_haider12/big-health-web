<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Models\Laboratory;
use App\Models\LaboratoryTest;
use App\Models\TestCategory;
use App\Models\Cart;
use App\Models\Contact;
use App\Models\Category;
use App\Models\TestCategoryDetail;
use App\Models\Banner;
use App\Models\FeaturedService;
use Auth;
use Storage;
use Hash;
use DataTables;

class HomeController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function homePage()
    {
        $data = [
            'featured_services' => FeaturedService::get(),
        ];
        return view('index',$data);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function aboutPage()
    {
        return view('About-us');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function contactUsPage()
    {
        return view('contact');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function contactUsMessage(Request $request)
    {
        $data = $request->validate([
            'name'      =>  'required|max:255',
            'email'     =>  'required|email|max:255',
            'message'   =>  'required|string',
        ]);
        if(Auth::check())
        {
            $data['user_id'] = auth()->user()->id;
        }
        Contact::create($data);

        return redirect()->back()->with('message','Thanks for Contact Us');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function laboratory(Request $request)
    {
         $data = [
            'all_laboratories'  =>   Laboratory::where('status',1)->paginate(9),
        ];
        if($request->keyword)
        {
            $data['all_laboratories'] = Laboratory::where([['status',1],['name','LIKE','%'.$request->keyword.'%']])->paginate(9);
        }
        // return $data;
        return view('find-laboratry',$data);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function tests(Request $request)
    {
         $data = [
            'all_tests'  =>   LaboratoryTest::where([['status',1]])->paginate(9),
            'banners'    =>   Banner::where('type','test')->get(),
        ];
        if($request->keyword)
        {
            $data['all_tests'] = LaboratoryTest::where([['status',1],['name','LIKE','%'.$request->keyword.'%']])->paginate(9);
            // $data['all_tests'] = TestCategory::with('test','laboratory')->whereIn('test_id',$test_ids)->paginate(9);
        }
        // return $data;
        return view('find-test',$data);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function testLaboratory($test)
    {
         $data = [
            'test_laboratories'  =>   TestCategory::with('test','laboratory')->where([['test_id',$test],['status',1]])->paginate(9),
            'banners'            =>   Banner::where([['type','test'],['foreign_id',$test]])->get(),
        ];
        // return $data;
        return view('test-laboratories',$data);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function categoryTest(Request $request, Category $category)
    {
         $data = [
            'category_tests'    =>   TestCategoryDetail::with('test','testCategory.laboratory')->where('category_id',$category->id)->paginate(9),
            'category'           =>   $category,
        ];
        if($request->keyword)
        {
            $test_ids = LaboratoryTest::where([['status',1],['name','LIKE','%'.$request->keyword.'%']])->get()->pluck('id')->toArray();
            $data['category_tests'] = TestCategoryDetail::with('test','testCategory.laboratory')->where('category_id',$category->id)->whereIn('test_id',$test_ids)->paginate(9);
        }
        return view('test-category',$data);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function laboratoryTest(Request $request, Laboratory $laboratory)
    {
         $data = [
            'laboratory_tests'    =>   TestCategory::with('test','laboratory')->where('foreign_id',$laboratory->id)->paginate(9),
            'laboratory'          =>   $laboratory,
            'banners'             =>   Banner::where([['type','laboratory'],['foreign_id',$laboratory->id]])->get(),
        
        ];
        // return $data;
        if($request->keyword)
        {
            $test_ids = LaboratoryTest::where([['status',1],['name','LIKE','%'.$request->keyword.'%']])->get()->pluck('id')->toArray();
            $data['category_tests'] = TestCategoryDetail::with('test','testCategory.laboratory')->where('category_id',$category->id)->whereIn('test_id',$test_ids)->paginate(9);
        }
        return view('test-laboratory',$data);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function cart()
    {
        $data = [
            'carts' =>  Cart::with('test_category.test')->where('user_id',Auth::user()->id)->get(),
        ];
        // return $data;
        return view('mycart',$data);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function cartDelete($id)
    {
        Cart::find($id)->delete();
        return redirect()->route('cart');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function testDetail($test)
    {
        $test_detail = TestCategory::with('laboratory','test','includeTests')->where('id',$test)->first();
        $data = [
            'test_detail' =>    $test_detail,
            'banners'     =>    Banner::where([['type','test'],['foreign_id', $test_detail->test_id]])->get(),

        ];
        // return $data;
        return view('Test-detail',$data);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function checkout()
    {
        $data = [
            'carts' =>  Cart::with('test_category.test')->where('user_id',Auth::user()->id)->get(),
        ];
        return view('checkout',$data);
    }
}
