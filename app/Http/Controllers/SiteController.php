<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Models\Laboratory;
use App\Models\LaboratoryTest;
use Auth;
use Storage;
use Hash;
use App\Models\OTP;
use App\Models\Cart;
use DataTables;

class SiteController extends Controller
{

    public function login(Request $request){
        $request->validate([
            'phone'     => 'required|exists:users,phone',
            'password'  => 'required|array'
        ]);
        
        $password = implode(',',$request->password);
        $password = str_replace(',','',$password);
        $user = User::where('phone',$request->phone)->first();
        
        if (Auth::attempt(['phone' => $request->phone, 'password' => $password], $request->get('remember'))) {
            Auth::login(User::where('phone',$request->phone)->first()); 
            return redirect('/');
        }
        return redirect()->back()->with('error','Invalid Credentials');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function otp(Request $request)
    {
        $request->validate([
            'id'        =>  'required',
            'contact'   =>  'required|max:11'
        ]);
        OTP::where([['user_id',Auth::user()->id],['phone',$request->contact]])->delete();
        $code = mt_rand(1000, 9999);
        OTP::create([
            'code'=>$code,
            'type'=>'cart',
            'user_id'=>Auth::user()->id,
            'phone'=>$request->contact
        ]);
        $data=['code'=>$code];

        return response()->json([
            'message' => 'success',
            'data'=>$data
        ], 200);
    }

    public function verifyOtp(Request $request){
        // return $request->all();
        $data = $request->validate([
            'id'        =>  'required',
            'test_id'   =>  'required',
            'code'      =>  'required'
        ]);
        // $user=User::where('phone',$data['phone'])->first();
        $code = OTP::where('user_id',Auth::user()->id)->where('code', $request->code)->first();
        if($code == '')
        {
            return response()->json([
                'error'=>'errors',
                'message' => 'Invalid Otp'
            ], 409);
        }
        $code->delete();
        $user_id = Auth::user()->id;
        $exist   = Cart::where([['user_id',$user_id],['test_id',$data['test_id']]])->first();
        $test    = LaboratoryTest::where('id',$data['test_id'])->first();
        if(!empty($exist))
        {
            return response()->json([
                'error'=>'errors',
                'message' => 'Test already exist in your cart'
            ], 409);
        }
        $data['user_id'] = $user_id;
        $data['amount']  = 100;
        Cart::create($data);
      
        return response()->json([
            'message' => 'success',
            'data'=>'cart Successfully'
        ], 200);
    }

    public function pin(){
        return view('5digint_pin');
    }

    public function pinSet(Request $request){
        $password = implode(',',$request->password);
        $password = str_replace(',','',$password);

        $password_confirm = implode(',',$request->password_confirm);
        $password_confirm = str_replace(',','',$password_confirm);

        if($password != $password_confirm)
        {
            return redirect()->back()->with('error','Your Pin does not match');
        }

        $pass = Hash::make($password);
        $user = Auth::user();
        $user->update(['password'=>$pass]);
        return redirect('/');
    }

    public function thankyou()
    {
        return view('thank-you');
    }
}
