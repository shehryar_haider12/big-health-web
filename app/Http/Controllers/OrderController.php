<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Models\Order;
use App\Models\OrderDetail;
use Auth;
use Storage;
use Hash;
use App\Models\Cart;
use App\Models\Checkout;
use DataTables;

class OrderController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function order(Request $request)
    { 
        // return $request->all();
        $data = $request->validate([
            'test_id'           =>  'required|array',
            'amount'            =>  'required|array',
            'payment_method'    =>  'required',
            'address'           =>  'required',
            'optional_address'  =>  'nullable',
            'email'             =>  'nullable|email',
            'phone'             =>  'required|max:11',
        ]); 
        $today              =   date("Ymd");
        $rand               =   strtoupper(substr(uniqid(sha1(time())),0,4));
        $order_no           =   $today . $rand;
        $data['order_no']   =   $order_no;
        $data['user_id']    =   Auth::user()->id;
            // return $data;
        $checkout = Checkout::create($data);

        foreach ($request->test_id as $key => $test) {
            $order_data = [
                'order_no'          => $order_no,
                'user_id'           => $data['user_id'],
                'test_id'           => $test,
                'checkout_id'       => $checkout->id,
                'amount'            => $request->amount[$key],
                'laboratory_id'     => $request->laboratory_id[$key]
            ];
            OrderDetail::create($order_data);
        }
        $order = [
            'order_no'      =>  $order_no,
            'total_amount'  =>  array_sum($request->amount),
            'checkout_id'       => $checkout->id,
        ];
        Order::create($order);
        Cart::where('user_id',Auth::user()->id)->delete();
        return redirect()->route('thankyou');
    }
}
