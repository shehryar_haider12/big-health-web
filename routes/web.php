<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', 'HomeController@homePage');
Route::get('/home', 'HomeController@homePage')->name('home');
Route::get('/about', 'HomeController@aboutPage')->name('about');
Route::get('/contact-us', 'HomeController@contactUsPage')->name('contact.us');
Route::post('/contact-msg', 'HomeController@contactUsMessage')->name('contact.msg');
Route::get('/laboratory', 'HomeController@laboratory')->name('laboratories');
Route::get('/laboratory/{laboratory}/tests', 'HomeController@laboratoryTest')->name('laboratory-test');
Route::get('/test', 'HomeController@tests')->name('tests');
Route::get('/test/{test}/laboratory', 'HomeController@testLaboratory')->name('test.laboratory');
Route::get('/test/detail/{test}', 'HomeController@testDetail')->name('test.detail');
Route::post('/login/user', 'SiteController@login')->name('login.user');

Route::middleware(['auth'])->group(function () {
    Route::get('/cart', 'HomeController@cart')->name('cart');
    Route::get('/cart/delete/{id}', 'HomeController@cartDelete')->name('cart.delete');
    Route::get('/checkout', 'HomeController@checkout')->name('checkout');
    Route::post('/order', 'OrderController@order')->name('order');
    Route::get('/thank-you', 'SiteController@thankyou')->name('thankyou');
    Route::post('/generate-otp', 'SiteController@otp')->name('generate.otp');
    Route::post('/verify-otp', 'SiteController@verifyOtp')->name('verify.otp');
    Route::get('/pin', 'SiteController@pin')->name('pin');
    Route::post('/pin/set', 'SiteController@pinSet')->name('pin.set');
    Route::get('/category/{category}/tests', 'HomeController@categoryTest')->name('category-test');
});    
Auth::routes();
// Cache Cleared Route
Route::get('exzed/cache', function() {
    Artisan::call('cache:clear');
    Artisan::call('view:clear');
    Artisan::call('route:clear');
    Artisan::call('config:clear');
   return 'cache clear';
});


