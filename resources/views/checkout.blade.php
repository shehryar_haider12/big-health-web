@extends('layouts.master')
@section('content')

<!--&lt;!&ndash; HOME &ndash;&gt;-->
<section id="home" class="slider" data-stellar-background-ratio="0.5">
    <div class="container">
        <div class="row">

            @php
                $sliders = sliders();
            @endphp
            <div class="owl-carousel owl-theme">
                @foreach ($sliders as $slider)
                <div class="item item-first" style="background-image: url({{env('CMS_URL').'/uploads/'}}{{$slider->image ?? 'placeholder.jpg'}})">
                    <div class="caption">
                        <div class="col-md-offset-1 col-md-10">
                                <h3>{{$slider->heading}}</h3>
                                <h1>{{$slider->text}}</h1>
                                <!--<a href="#team" class="section-btn btn btn-default smoothScroll">Meet Our Doctors</a>-->
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </div>
</section>
<style>


</style>
<section id="specimen_collection_sec">
    <div class="container">
        <form action="{{route('order')}}" method="post">
            @csrf
        <div class="row">
            <div class="col-md-5 ">
                <h4>DETAILS FOR CHECKOUT</h4>
                <p>Required Feilds</p>
                <div class="form__group field">
                    <label>Full Name</label>
                    <input type="input" class="form__field" placeholder="Full Name" value="{{Auth::user()->name ?? null}}" name="name" id='name' required />
                    <span class="text-danger">{{$errors->first('naem') ?? old('name')}}</span>

                </div>
                {{-- <div class="form__group field">
                    <label>City</label>
                    <select class="form-select form__field" aria-label="Default select example">
                        <option selected disabled>Select City</option>
                        <option>Karachi</option>
                        <option value="1">Lahore</option>
                        <option value="2">Islamabad</option>

                    </select>

                </div> --}}
                <div class="form__group field">
                    <label>Street Address</label>
                    <input type="input" class="form__field" placeholder="Address" value="" name="address" id='address' required />
                    <span class="text-danger">{{$errors->first('address') ?? old('address')}}</span>

                </div>

                <div class="form__group field">
                    <label>Optional Address</label>
                    <input type="input" class="form__field" placeholder="Optional Address"  value="" name="optional_address" id='optional_address' />
                    <span class="text-danger">{{$errors->first('optional_address') ?? old('optional_address')}}</span>

                </div>
                <div class="seprater col-md-offset-3"></div>
                <h4>CONTACT INFORMATION</h4>

                <div class="form__group field">
                    <label>Email</label>
                    <input type="input" class="form__field" value="{{Auth::user()->email ?? null}}" placeholder="Enter Email (optional)" name="email" id='email' />
                    <span class="text-danger">{{$errors->first('email') ?? old('email')}}</span>

                </div>
                <div class="form__group field">
                    <label>Phone Number</label>
                    <input type="input" class="form__field" value="{{Auth::user()->phone ?? null}}"  placeholder="Enter Phone Number" name="phone" id='phone' required />
                    <span class="text-danger">{{$errors->first('phone') ?? old('phone')}}</span>

                </div>


        </div>

            <div class="col-md-5 recpt_box col-md-offset-1">

                <h5>Receipt</h5>
                <p class="Recipt_des">Test<span class="span_feild">Total</span></p>

                @foreach ($carts as $cart)
                    <input type="hidden" name="test_id[]" value="{{$cart->test_id}}">
                    <input type="hidden" name="amount[]" value="{{$cart->amount}}">
                    <input type="hidden" name="laboratory_id[]" value="{{$cart->test_category->laboratory->id}}">
                    <p class="Recipt_des">{{$cart->test_category->test->name}} ({{$cart->test_category->laboratory->name}})<span class="span_feild">Rs.{{$cart->amount}}</span></p>
                @endforeach
                @php
                    $total = $carts->pluck('amount')->toArray();
                    $total_amount = array_sum($total);
                @endphp
                <p class="Recipt_des">Total<span class="span_feild">Rs.{{$total_amount ?? 0}}</span></p>
                <h5>Payment Method</h5>
                <div class="form-check">
                    <input class="form-check-input" name="payment_method" type="checkbox" value="cash_delivery" id="flexCheckDefault">
                    <label class="form-check-label" for="flexCheckDefault">
                       Cash On Delivery
                    </label>
                </div>
                <p>Lorem ispum Lorem ispum Lorem ispum Lorem ispum Lorem ispum Lorem ispum
                    Lorem ispum Lorem ispum Lorem ispum Lorem ispum Lorem ispum
                </p>
                <button type="submit" class="btn btn-secondary btn_confirm">Confirm</button>

            </div>
        </form>
    </div>
</div>
</section>
@endsection