@extends('layouts.master')
@section('content')

<!--&lt;!&ndash; HOME &ndash;&gt;-->
<section id="home" class="slider" data-stellar-background-ratio="0.5">
    <div class="container">
        <div class="row">
            @php
                $sliders = sliders();
            @endphp
            <div class="owl-carousel owl-theme">
                @foreach ($sliders as $slider)
                <div class="item item-first" style="background-image: url({{env('CMS_URL').'/uploads/'}}{{$slider->image ?? 'placeholder.jpg'}})">
                        <div class="caption">
                            <div class="col-md-offset-1 col-md-10">
                                <h3>{{$slider->heading}}</h3>
                                <h1>{{$slider->text}}</h1>
                                <!--<a href="#team" class="section-btn btn btn-default smoothScroll">Meet Our Doctors</a>-->
                            </div>
                        </div>
                </div>
                @endforeach
            </div>

        </div>
    </div>
</section>


<section id="my_cart_sec">
    <div class="container">
        @if ($carts->count() > 0)
        
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <h2>{{auth()->user()->name ?? null}}</h2>
                <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">

                    <div class="panel panel-default">
                        @php
                            $i =1;
                        @endphp
                        @foreach ($carts as $cart)
                            <div class="panel-heading" role="tab" id="tab{{$cart->id}}">
                                <h4 class="panel-title">
                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion"
                                        href="#collapse{{$cart->id}}" aria-expanded="false" aria-controls="collapse{{$cart->id}}">
                                        {{$cart->test_category->test->name}}
                                    </a>
                                    <span class="cart_price">{{$cart->amount}} PKR 
                                        <a class="collapsed" role="button" href="{{route('cart.delete',$cart->id)}}"><i class="fa fa-minus"></i>
                                        </a> </span>
                                </h4>
                            </div>
                            <div id="collapse{{$cart->id}}" class="panel-collapse collapse" role="tabpanel" aria-labelledby="tab{{$cart->id}}">
                                <div class="panel-body">
                                    {{$cart->test_category->test->description}}
                                </div>
                            </div>
                        @endforeach


                        {{-- <div class="panel-heading" role="tab" id="tab2">
                <h4 class="panel-title">
                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                       Blood C/E
                    </a>
                    <span class="cart_price">20,000 PKR    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo"><i class="fa fa-minus"></i> </a> </span>
                </h4>
            </div>
            <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="tab2">
                <div class="panel-body">
                lorem Ispum
                </div>
            </div> --}}


                    </div>


                </div>
            </div>
            @php
            $total = $carts->pluck('amount')->toArray();
            $total_amount = array_sum($total);
            @endphp
            <div class="col-md-10 col-md-offset-1 sub_total_bar">
                <h6 class="bar_details">Total Test <span class="span_amount">{{$total_amount ?? 0}} PKR</span></h6>
                <p>Details</p>
                <p>Lorum Ispum Lorum Ispum Lorum Ispum Lorum Ispum Lorum Ispum Lorum Ispum Lorum Ispum Lorum Ispum
                    Lorum Ispum Lorum Ispum Lorum Ispum Lorum Ispum Lorum Ispum Lorum Ispum Lorum Ispum Lorum Ispum
                </p>
                <a href="{{route('checkout')}}"> <button type="button" class="btn btn-primary process_btn">Proceed To Check Out</button></a>

            </div>

        </div>
        @else
        <div class="row" style="background: #8080801f;">
            <div class="col-md-10 col-md-offset-1">
                <h4 style="color:gray;">Cart is Empty</h4>
                <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                    <div class="panel panel-default">
                    </div>
                </div>
            </div>
        </div>
        @endif
    </div>
</section>


@endsection