@extends('layouts.master')
@section('content')
<!--&lt;!&ndash; HOME &ndash;&gt;-->
<section id="home" class="slider" data-stellar-background-ratio="0.5">
    <div class="container">
        <div class="row">

            @php
                $sliders = sliders();
            @endphp
            <div class="owl-carousel owl-theme">
                @foreach ($sliders as $slider)
                <div class="item item-first" style="background-image: url({{env('CMS_URL').'/uploads/'}}{{$slider->image ?? 'placeholder.jpg'}})">
                        <div class="caption">
                            <div class="col-md-offset-1 col-md-10">
                                <h3>{{$slider->heading}}</h3>
                                <h1>{{$slider->text}}</h1>
                                <!--<a href="#team" class="section-btn btn btn-default smoothScroll">Meet Our Doctors</a>-->
                            </div>
                        </div>
                </div>
                @endforeach
            </div>

        </div>
    </div>
</section>


<section id="Login_sec" style="padding: 70px;">
   <div class="container">
    <form action="{{route('login.user')}}" method="post">
        @csrf
       <div class="row">
           <div class="col-md-7">
               <img src="{{url('')}}/assets/images/appointment-image.jpg" width="500px" height="400">
             </div>

           <div class="col-md-4 pin_row">
                @if (session('error'))
                    <span class="alert alert-danger">{{session('error')}}</span>
                @endif
               <div class="form__group field">
                   <input type="input" class="form__field" placeholder="Phone Number" name="phone" id="phone" required="">
                   <span class="text-danger">{{$errors->first('phone') ?? old('phone')}}</span>
               </div>

               <div class="pin-wrapper">
                   <label>Pin</label>
               <input type="password" data-role="pin" maxlength="1" name="password[]" class="pin-input">
               <input type="password" data-role="pin" maxlength="1" name="password[]" class="pin-input">
               <input type="password" data-role="pin" maxlength="1" name="password[]" class="pin-input">
               <input type="password" data-role="pin" maxlength="1" name="password[]" class="pin-input">
               <input type="password" data-role="pin" maxlength="1" name="password[]" class="pin-input">

           </div>
           
        </div>
        <div class="col-md-7"></div>
        <div class="col-md-5" style="position: relative;bottom: 95px;">
            <button type="submit" class="btn btn-secondary btn_registration" >Login</button>
            <br><hr>
            <a href="{{route('register')}}"><span class="password_links">Forget Your Password </span></a>
            <a href="{{route('register')}}"><span class="password_links">&nbsp;  Register Now </span></a>
        </div>
      </div>
    </form>
   </div>
</section>
@endsection