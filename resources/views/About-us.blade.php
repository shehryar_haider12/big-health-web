@extends('layouts.master')
@section('content')

     <!--&lt;!&ndash; HOME &ndash;&gt;-->
     <section id="home" class="slider" data-stellar-background-ratio="0.5">
          <div class="container">
               <div class="row">

                    {{-- @php
                         $sliders = sliders();
                    @endphp --}}
                    <div class="owl-carousel owl-theme">
                         <div class="item item-first" style="background-image: url({{url('').'/uploads/about-us.png'}})">
                              <div class="caption">
                                   <div class="col-md-offset-1 col-md-10">
                                        {{-- <h3>{{$slider->heading}}</h3>
                                        <h1>{{$slider->text}}</h1> --}}
                                        <!--<a href="#team" class="section-btn btn btn-default smoothScroll">Meet Our Doctors</a>-->
                                   </div>
                              </div>
                         </div>
                         {{-- @endforeach --}}
                    </div>

               </div>
          </div>
     </section>



     <section id="into_sec">
          <div class="container">
               <div class="row">
                    <div class="col-md-12">
                         <h1>What is big health?</h1>
                         <div class="row">
                              <!--<i class="fa fa-quote-left"></i>-->
                              <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
                              <!--<i class="fa fa-quote-right"></i>-->
                         </div>


                    </div>
               </div>
          </div>
     </section>
@endsection