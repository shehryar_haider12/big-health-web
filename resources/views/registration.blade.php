@extends('layouts.master')
@section('content')
<!--&lt;!&ndash; HOME &ndash;&gt;-->
<section id="home" class="slider" data-stellar-background-ratio="0.5">
    <div class="container">
        <div class="row">

            @php
                $sliders = sliders();
            @endphp
            <div class="owl-carousel owl-theme">
                @foreach ($sliders as $slider)
                <div class="item item-first" style="background-image: url({{env('CMS_URL').'/uploads/'}}{{$slider->image ?? 'placeholder.jpg'}})">
                    <div class="caption">
                        <div class="col-md-offset-1 col-md-10">
                                <h3>{{$slider->heading}}</h3>
                                <h1>{{$slider->text}}</h1>
                                <!--<a href="#team" class="section-btn btn btn-default smoothScroll">Meet Our Doctors</a>-->
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </div>
</section>


<section id="registration_form">
   <div class="container">
       <form action="{{route('register')}}" method="post">
        @csrf
       <div class="row">
           <div class="col-md-7">
            <img src="{{url('')}}/assets/images/appointment-image.jpg" width="500px" height="400">

           </div>
           <div class="col-md-4">
               <div class="registration_form">
                       <div class="form__group field">
                           <input type="input" class="form__field" placeholder="Full Name" name="name" id='name' required />
                            <p class="text-danger">{{$errors->first('name') ?? old('name')}}</p>
                       </div>
                   <div class="form__group field">
                       <input type="date" class="form__field" placeholder="DOB" name="dob" id='dob' required />

                   </div>
                       <div class="form__group field">
                           <select class="form-select form__field" name="gender" aria-label="Default select example">
                               <option selected disabled>Select Gender</option>
                               <option value="male">male</option>
                               <option value="female">Female</option>
                           </select>
                           <span class="text-danger">{{$errors->first('gender') ?? old('gender')}}</span>

                       </div>

                        <div class="form__group field">
                           <input type="input" class="form__field" placeholder="Email (Optional)" name="email" id='email' />
                           <span class="text-danger">{{$errors->first('email') ?? old('email')}}</span>

                       </div>
                       <div class="form__group field">
                           <input type="input" class="form__field" placeholder="Phone Number" name="phone" id='phone' required />
                           <span class="text-danger">{{$errors->first('phone') ?? old('phone')}}</span>

                       </div>
                   <button type="submit" class="btn btn-secondary btn_registration" >Registration</button>

                   <a href="{{route('login')}}"> <button type="button" class="btn btn-secondary btn_login">Log in</button></a>
                   <div class="form-check">
                       <input class="form-check-input" name="agree_terms" type="checkbox" id="flexCheckDefault">
                       <label class="form-check-label" for="flexCheckDefault">
                           I Agree to Terms and Conditions
                       </label>
                       <span class="text-danger">{{$errors->first('agree_terms') ?? old('agree_terms')}}</span>

                   </div>

                   </div>

               </div>
           </div>
        </div>
   
    </form>
    </div>
</section>
@endsection