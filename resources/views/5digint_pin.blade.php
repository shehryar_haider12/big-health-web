@extends('layouts.master')
@section('content')
<!--&lt;!&ndash; HOME &ndash;&gt;-->
<section id="home" class="slider" data-stellar-background-ratio="0.5">
    <div class="container">
        <div class="row">
            @php
                $sliders = sliders();
            @endphp
            <div class="owl-carousel owl-theme">
                @foreach ($sliders as $slider)
                <div class="item item-first" style="background-image: url({{env('CMS_URL').'/uploads/'}}{{$slider->image ?? 'placeholder.jpg'}})">
                    <div class="caption">
                        <div class="col-md-offset-1 col-md-10">
                                <h3>{{$slider->heading}}</h3>
                                <h1>{{$slider->text}}</h1>
                                <!--<a href="#team" class="section-btn btn btn-default smoothScroll">Meet Our Doctors</a>-->
                        </div>
                    </div>
                </div>
                @endforeach
            </div>

        </div>
    </div>
</section>


<section id="pass_pin" style="padding: 70px;">
   <div class="container">
       <form action="{{route('pin.set')}}" method="post">
        @csrf
       <div class="row">
           <div class="col-md-7">
               <img src="{{url('')}}/assets/images/appointment-image.jpg" width="500px" height="400">
             </div>
           <div class="col-md-5 pin_row">
               @if (session('error'))
                    <p class="alert alert-danger">{{session('error')}}</p>
               @endif
               
               <p class="title_bar">Select your 5 digints pin</p>

           <div class="pin-wrapper">
               <input type="password" name="password[]" data-role="pin" maxlength="1" class="pin-input">
               <input type="password" name="password[]" data-role="pin" maxlength="1" class="pin-input">
               <input type="password" name="password[]" data-role="pin" maxlength="1" class="pin-input">
               <input type="password" name="password[]" data-role="pin" maxlength="1" class="pin-input">
               <input type="password" name="password[]" data-role="pin" maxlength="1" class="pin-input">
           </div>
               <p class="title_bar">Reconfirm your 5 digints pin</p>

               <div class="pin-wrapper">
                   <input type="password" name="password_confirm[]" data-role="pin" maxlength="1" class="pin-input">
                   <input type="password" name="password_confirm[]" data-role="pin" maxlength="1" class="pin-input">
                   <input type="password" name="password_confirm[]" data-role="pin" maxlength="1" class="pin-input">
                   <input type="password" name="password_confirm[]" data-role="pin" maxlength="1" class="pin-input">
                   <input type="password" name="password_confirm[]" data-role="pin" maxlength="1" class="pin-input">
               </div>

               
            </div>
            <div class="col-md-7"></div>

            <div class="col-md-5">
            <button type="submit" class="btn btn-secondary btn_registration" >Confirm</button>

            <a href="{{route('register')}}"> <button type="button" class="btn btn-secondary btn_login">Go Back</button></a>
        </div>
    </div>
    </form>
</div>
</section>
@endsection