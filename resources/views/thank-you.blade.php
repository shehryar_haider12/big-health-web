@extends('layouts.master')
@section('content')
@section('custom-style')
<style>
    #thank_u .box {
        background: #4dcc71;
        padding-top: 80px;
        padding-bottom: 80px;
        border-radius: 20px;
        padding: 70px 260px 70px 260px;
    }
    #thank_u{
        padding-top: 50px;
        padding-bottom: 50px;
    }
    #thank_u h2{
    text-align: center;
    color: white;
    }
    #thank_u p{
    text-align: center;
    color: white;
    line-height: 3;}
</style>
@endsection
<!--&lt;!&ndash; HOME &ndash;&gt;-->
<section id="home" class="slider" data-stellar-background-ratio="0.5">
    <div class="container">
        <div class="row">

            @php
                $sliders = sliders();
            @endphp
            <div class="owl-carousel owl-theme">
                @foreach ($sliders as $slider)
                <div class="item item-first" style="background-image: url({{env('CMS_URL').'/uploads/'}}{{$slider->image ?? 'placeholder.jpg'}})">
                    <div class="caption">
                        <div class="col-md-offset-1 col-md-10">
                                <h3>{{$slider->heading}}</h3>
                                <h1>{{$slider->text}}</h1>
                                <!--<a href="#team" class="section-btn btn btn-default smoothScroll">Meet Our Doctors</a>-->
                        </div>
                    </div>
                </div>
                @endforeach
            </div>

        </div>
    </div>
</section>


<section id="thank_u">
   <div class="container">
       <div class="row">
           <div class="col-md-10 box col-md-offset-1">
               <h2>Thank You!</h2>
               <p>Your Details has been recieved our representative will contact you soon!
                </p>
                <hr>  
               <p> <a class="text-center" href="{{route('home')}}">Go back</a></p>
            </div>

        </div>
   </div>
</section>
@endsection