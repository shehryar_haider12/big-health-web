@extends('layouts.master')
@section('content')
@section('custom-style')
<style>
    #contact_page_sec input[type=text] {
        border: none;
        border-bottom: 1px solid #8842d5;
        margin-top: 7px;
        border-radius: 0px;
    }


    #contact_page_sec input[type=text] {
        border: none;
        border-bottom: 1px solid #1b1a1d;
        margin-top: 69px;
        border-radius: 0px;
    }


    #contact_page_sec .form__field {
        font-family: inherit;
        width: 100%;
        border: 0;
        outline: 0;
        font-size: 1.3rem;
        color: #4dcc71;
        padding: 18px;
        transition: border-color 0.2s;
        padding-bottom: 23px;
        background: #f9f9f9;
        border-radius: 14px;
        margin-bottom: 11px;
    }
    #contact_page_sec.form__group.field {
        /* padding: 27px; */
        padding-left: 15px;
        padding-right: 14px;
        padding-top: 5px;
        padding-bottom: 5px;
    }
    #contact_page_sec label{
        padding-top: 32px;
        margin-left: 10px;
        margin-bottom: 15px;
    }
    #contact_page_sec .seprater
    { background: #e8e6e6;
        height: 2px;
        width: 50%;
        margin-top: 44px;
    }
    #contact_page_sec {
        padding-top: 75px;
        padding-bottom: 50px;}

    #contact_page_sec .Recipt_des{
        border-bottom: 2px solid #dadada;
        padding-bottom: 10px;
        color: #7e7e92;
    }
    #contact_page_sec .span_feild{
        float: right;
        color: #4dcc71;
    }
    #contact_page_sec .btn_registration{
        padding: 15px 164px 14px;
        background: #4dcc71;
        color: white;
        border-radius: 23px;
    }

    #contact_page_sec .btn_login {
        padding: 12px 50px 14px;
        background: transparent;
        color: #4dcc71;
        border-radius: 23px;
        margin-left: 12px;
        border: 1px solid #4dcc71;}
</style>

@endsection
<!--&lt;!&ndash; HOME &ndash;&gt;-->
<section id="home" class="slider" data-stellar-background-ratio="0.5">
    <div class="container">
        <div class="row">

            {{-- @php
                $sliders = sliders();
            @endphp --}}
            <div class="owl-carousel owl-theme">
                <div class="item item-first" style="background-image: url({{url('').'/uploads/contact-us.png'}})">
                     <div class="caption">
                          <div class="col-md-offset-1 col-md-10">
                               {{-- <h3>{{$slider->heading}}</h3>
                               <h1>{{$slider->text}}</h1> --}}
                               <!--<a href="#team" class="section-btn btn btn-default smoothScroll">Meet Our Doctors</a>-->
                          </div>
                     </div>
                </div>
                {{-- @endforeach --}}
           </div>

        </div>
    </div>
</section>


<section id="contact_page_sec">
   <div class="container">
       <div class="row">
           <form action="{{route('contact.msg')}}" method="post">
            @csrf
           <div class="col-md-7">
                <img src="{{url('')}}/assets/images/appointment-image.jpg" width="500px" height="400">

           </div>
           <div class="col-md-4">
                @if (session('message'))
                    <p class="alert alert-success">{{session('message')}}</p>
                @endif
               <div class="registration_form">


                       <div class="form__group field">
                           <input type="input" class="form__field" placeholder="Name" name="name" id='name' required />
                           <span class="text-danger">{{$errors->first('name') ?? old('name')}}</span>

                       </div>
                   <div class="form__group field">
                       <input type="input" class="form__field" placeholder="Email" name="email" id='email' required />
                       <span class="text-danger">{{$errors->first('email') ?? old('email')}}</span>

                   </div>

                   <div class="form__group field">

                       <textarea class="form-control form__field" name="message" rows="7" id="comment" placeholder="Message"></textarea>
                       <span class="text-danger">{{$errors->first('message') ?? old('message')}}</span>

                   </div>

                   <button type="submit" class="btn btn-secondary btn_registration" data-dismiss="modal">Send</button>



                   </div>

               </div>
                </form>
           </div>
        </div>
   </div>
</section>
@endsection
