<!-- HEADER -->
<header>
    <div class="container">
         <div class="row">

              <div class="col-md-8 col-sm-5">
                   <p><span style="color: #4dcc71;
   margin-right: -9px;"><i class="fa fa-heart" aria-hidden="true"></i></span>Welcome to Big health We Provides
                        <strong>laboratreis Service</strong> For More Information Call </span> <button type="button"
                             class="btn btn-primary callbtn"><i class="fa fa-phone"
                                  aria-hidden="true"></i>&nbsp;+123(456)789</button> </p>


              </div>

              <div class="col-md-4 col-sm-5 text-align-right">
                   @if (Auth::check() && !empty(Auth::user()->password))
                   <form action="{{route('logout')}}" method="post"> 
                         @csrf
                         <a href=""> <span class="phone-icon"> Welcome {{Auth::user()->name}} !</span></a>
                         <button style="background: no-repeat;border: none;" type="submit"><span class="date-icon"><i class="fa fa-sign-out"></i> Logout</span></button>
                    </form>
                   @else
                         <a href="{{route('login')}}"> <span class="phone-icon"><i class="fa fa-lock"></i> Sign in</span></a>
                         <a href="{{route('register')}}"> <span class="date-icon"><i class="fa fa-user-plus"></i> Sign up</span></a>    
                   @endif
              </div>

         </div>
    </div>
</header>
