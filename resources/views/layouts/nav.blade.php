<!-- MENU -->
<section class="navbar navbar-default navbar-static-top" role="navigation">
    <div class="container">

         <div class="navbar-header">
              <button class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                   <span class="icon icon-bar"></span>
                   <span class="icon icon-bar"></span>
                   <span class="icon icon-bar"></span>
              </button>

              <!-- lOGO TEXT HERE -->
              <a href="index.html" class="navbar-brand"><img src="{{url('')}}/assets/images/web/BigHealth%20Logo.png" class="logo"></a>
         </div>

         <!-- MENU LINKS -->
         <div class="collapse navbar-collapse">
              <ul class="nav navbar-nav navbar-right">
                   <li><a href="{{route('home')}}" class="smoothScroll {{Route::currentRouteName() == 'home' ? 'active' : null}}">Home</a></li>
                   <li><a href="{{route('laboratories')}}" class="smoothScroll {{Route::currentRouteName() == 'laboratories' || Route::currentRouteName() == 'laboratory-test' ? 'active' : null}}">Laboratory</a></li>
                   <li><a href="{{route('tests')}}" class="smoothScroll {{Route::currentRouteName() == 'tests' | Route::currentRouteName() == 'test.detail' ? 'active' : null}}">All Tests</a></li>
                   <li><a href="{{route('about')}}" class="smoothScroll {{Route::currentRouteName() == 'about' ? 'active' : null}}">About Us</a></li>
                   <li><a href="{{route('contact.us')}}" class="smoothScroll {{Route::currentRouteName() == 'contact.us' ? 'active' : null}}">Contact</a></li>
                   <li>
                        <!-- Actual search box -->
                        <div class="form-group has-search">
                             <span class="fa fa-search form-control-feedback"></span>
                             <input type="text" class="form-control" placeholder="Search">
                        </div>
                   </li>
                   <li class="appointment-btn"><a href="{{route('cart')}}"><i class="fa"
                                  style="font-size:16px;">&#xf07a;</i>
                             <span class='badge badge-warning' id='lblCartCount'> {{cart_count()}} </span>&nbsp; My Cart</a></li>
              </ul>
         </div>

    </div>
</section>
