<!DOCTYPE html>
<html lang="en">
<head>

     <title>Health - Medical Website Template</title>
     <meta charset="UTF-8">
     <meta http-equiv="X-UA-Compatible" content="IE=Edge">
     <meta name="description" content="">
     <meta name="keywords" content="">
     <meta name="author" content="Tooplate">
     <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
     {{-- SweetAlert2 --}}
     <link rel="stylesheet" href="{{url('')}}/assets/plugins/sweet-alert2/sweetalert2.min.css">
     <link rel="stylesheet" href="{{url('')}}/assets/css/bootstrap.min.css">
     <link rel="stylesheet" href="{{url('')}}/assets/css/font-awesome.min.css">
     <link rel="stylesheet" href="{{url('')}}/assets/css/animate.css">
     <link rel="stylesheet" href="{{url('')}}/assets/css/owl.carousel.css">
     <link rel="stylesheet" href="{{url('')}}/assets/css/owl.theme.default.min.css">

     <!-- MAIN CSS -->
     <link rel="stylesheet" href="{{url('')}}/assets/css/tooplate-style.css">
     <link rel="stylesheet" href="{{url('')}}/assets/css/custom.css">
     @yield('custom-style')
     <style>
          /* .swal2-modal.swal2-show{
               width: 350px !important;
          } */
          #lab_records .search_bt {
               position: relative;
               /* margin-top: -57px; */
               left: 78%;
               bottom: 36px;
               border: 1px solid #4dcc71;
               background: transparent;
               color: black;
          }
          a.smoothScroll.active {
               background: #4caf50c2;
               color: white !important;
               border-radius: 6px;
          }
     </style>
</head>
<body id="top" data-spy="scroll" data-target=".navbar-collapse" data-offset="50">
     <!-- PRE LOADER -->
     <section class="preloader">
          <div class="spinner">

               <span class="spinner-rotate"></span>
               
          </div>
     </section>
     @include('layouts.header')
     @include('layouts.nav')
     @yield('content')
     @include('layouts.footer')

  <!-- SCRIPTS -->
  <script src="{{url('')}}/assets/js/jquery.js"></script>
  <script src="{{url('')}}/assets/js/bootstrap.min.js"></script>
  {{-- SweetAlert2 --}}
<script src="{{url('')}}/assets/plugins/sweet-alert2/sweetalert2.min.js"></script>
  <script src="{{url('')}}/assets/js/jquery.sticky.js"></script>
  <script src="{{url('')}}/assets/js/jquery.stellar.min.js"></script>
  <script src="{{url('')}}/assets/js/wow.min.js"></script>
  <script src="{{url('')}}/assets/js/smoothscroll.js"></script>
  <script src="{{url('')}}/assets/js/owl.carousel.min.js"></script>
  <script src="{{url('')}}/assets/js/custom.js"></script>
  {{-- Axios --}}
<script src="{{url('')}}/assets/custom/js/axios.min.js"></script>
  @yield('custom-script')
  <script>
    $(':radio').change(function() {
        console.log('New star rating: ' + this.value);
    });
</script>
</body>
</html>