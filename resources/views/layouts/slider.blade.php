<div id="myCarousel" class="carousel slide" data-ride="carousel">
    <!-- Indicators -->
    <ol class="carousel-indicators">
        <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
        <li data-target="#myCarousel" data-slide-to="1"></li>
        <li data-target="#myCarousel" data-slide-to="2"></li>
    </ol>

    <!-- Wrapper for slides -->
    <div class="carousel-inner">
        <div class="item active">
            <img src="{{url('assets/')}}/images/web/banner.jpg" alt="banner">
        </div>

        <div class="item">
            <img src="{{url('assets/')}}/images/web/banner.jpg" alt="Chicago">
        </div>


    </div>
</div>
