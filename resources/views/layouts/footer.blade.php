
<!-- FOOTER -->
<footer data-stellar-background-ratio="5">
    <div class="container">
         <div class="row">

              <div class="col-md-3 col-sm-3">
                   <div class="footer-thumb">
                        <h3 class="wow fadeInUp" data-wow-delay="0.4s">Terms of use</h3>
                        <p>Big Health is offering free home sample collection services to the patients to help them get tested in the convenience of their homes. To book home sample collection call 03-111 456 789.</p>


                   </div>
              </div>

              <div class="col-md-3 col-sm-3">
                   <div class="footer-thumb">
                        <h3 class="wow fadeInUp" data-wow-delay="0.4s">Contact Info</h3>
                        <p style="    border: 1px solid #80808057;padding: 7px;">Call for Free Home Sample Collection.</p>
                        <p style="    border: 1px solid #80808057;padding: 7px;">Big Health Home Care 24/7.</p>
                        <p style="    border: 1px solid #80808057;padding: 7px;">Big Health Lab Blue Card Discount.</p>
                        <p style="    border: 1px solid #80808057;padding: 7px;">Big Health Medical Center.</p>
                        <p style="    border: 1px solid #80808057;padding: 7px;">Big Health Medical Center.</p>


                   </div>
              </div>

              <div class="col-md-3 col-sm-3">
                   <div class="footer-thumb">
                        <h3 class="wow fadeInUp" data-wow-delay="0.4s">Subscribe</h3>
                        <p>Please Subscribe for our latest updates .</p>
                        <form action="{{route('laboratories')}}" method="get">
                              <input type="text" name="keyword" class="form-control" style="border-radius: 0px" placeholder="please enter your email">
                              <button style="    margin: 10px; background: #00800094;" type="submit" class="btn-success btn-sm">Subscribe</button>
                         </form>

                   </div>
              </div>

              <div class="col-md-3 col-sm-3">
                   <div class="footer-thumb">
                        <h3 class="wow fadeInUp" data-wow-delay="0.4s">About</h3>
                        <p>Big Health is one of the leading pathology labs in Pakistan. The Big Health team has a mission to deliver accurate results, on time. The lab operates 24 hours a day, every day of the year.</p>
                   </div>
              </div>


              <div class="col-md-12 col-sm-12 border-top">

                   <div class="col-md-5 col-sm-5 col-md-offset-7">
                        <div class="footer-link">
                             <a href="{{route('home')}}">Home </a>
                             <a href="{{route('about')}}">About</a>
                             <a href="{{route('contact.us')}}">Contact</a>
                             @if (Auth::check() && !empty(Auth::user()->password))
                             {{-- <a href=""> <span> Welcome {{Auth::user()->name}} !</span></a> --}}
                              <form action="{{route('logout')}}" method="post" style="display: inline-flex;margin-top: 5px;color: green;"> 
                                   @csrf
                                   <button style="background: no-repeat;border: none;" type="submit"><span class="date-icon"><i class="fa fa-sign-out"></i> Logout</span></button>
                              </form>
                              @else
                                   <a href="{{route('login')}}"> <span><i class="fa fa-lock"></i>Sign in</span></a>
                                   <a href="{{route('register')}}"> <span><i class="fa fa-user-plus"></i>Sign up</span></a>    
                              @endif
                             {{-- <a href="#"><span><i class="fa fa-lock"></i>Sign in</span></a>
                             <a href="#"><span><i class="fa fa-user-plus"></i>Sign up</span></a> --}}
                        </div>
                   </div>
                   <div class="col-md-2 col-sm-2 text-align-center">
                        <div class="angle-up-btn">
                             <a href="#top" class="smoothScroll wow fadeInUp" data-wow-delay="1.2s"><i
                                       class="fa fa-angle-up"></i></a>
                        </div>
                   </div>
              </div>

         </div>
    </div>
</footer>