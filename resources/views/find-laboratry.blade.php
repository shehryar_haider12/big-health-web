@extends('layouts.master')
@section('content')
@section('custom-style')
<style>
.col-md-12.btn_view.text-center:hover {
     background: #8bc34a61 !important;
 }
</style>
@endsection

<!--&lt;!&ndash; Find Laboratory &ndash;&gt;-->

<section id="home" class="slider" data-stellar-background-ratio="0.5">
     <div class="container">
          <div class="row">

               {{-- @php
                    $sliders = sliders();
               @endphp --}}
               <div class="owl-carousel owl-theme">
                    <div class="item item-first" style="background-image: url({{url('').'/uploads/lab.png'}})">
                         <div class="caption">
                              <div class="col-md-offset-1 col-md-10">
                                   {{-- <h3>{{$slider->heading}}</h3>
                                   <h1>{{$slider->text}}</h1> --}}
                                   <!--<a href="#team" class="section-btn btn btn-default smoothScroll">Meet Our Doctors</a>-->
                              </div>
                         </div>
                    </div>
                    {{-- @endforeach --}}
               </div>

          </div>
     </div>
</section>

<section id="lab_records">

     <div class="container">
          <div class="row">

               <!--<startbuttonsRow>-->
               <div class="col-md-4 pd-10-mb"><a href="#tab_All_lab" data-toggle="pill"><button type="button"
                              class="active btn btn-primary all_lab_btn">All laboratory</button></a></div>

               <div class="col-md-4"><a href="#tab_by_generic" data-toggle="pill"><button type="button"
                              class="btn btn-primary by_generic_btn">By Generic</button></a></div>

               <div class="col-md-4">
                    
                    
                    <form action="{{route('laboratories')}}" method="get">
                         <input type="text" name="keyword" class="form-control lab_search lab_search" placeholder="Search">
                         <button type="submit" class="btn-success btn-sm search_bt">search</button>
                    </form>
                    {{-- <a href="#search" data-toggle="pill">  --}}
                         
                    {{-- </a> --}}
               </div>

               <!--<endbuttonsRow>-->

               <div class="tab-content">

                    <div class="tab-pane active" id="tab_All_lab">
                         <!--<all lab Data >-->
                              {{-- {{dd(env('CMS_URL'))}} --}}
                         <div class="row inner_col">
                              @foreach ($all_laboratories as $laboratory)
                              {{-- {{dd($laboratory)}} --}}
                              <div class="col-md-4 pd-tp">
                                   <div class="info-box">
                                        <p class="text-center"><img
                                                  src="{{env('CMS_URL').'/uploads/'.$laboratory->image}}"
                                                  class="img_round"></p>
                                        <h1>{{$laboratory->name}}<br>
                                             <span class="describtion">{{$laboratory->address}}</span>
                                        </h1>
                                        <form class="rating">
                                             <label>
                                                  <input type="radio" name="stars" value="1" />
                                                  <span class="icon">★</span>
                                             </label>
                                             <label>
                                                  <input type="radio" name="stars" value="2" />
                                                  <span class="icon">★</span>
                                                  <span class="icon">★</span>
                                             </label>
                                             <label>
                                                  <input type="radio" name="stars" value="3" />
                                                  <span class="icon">★</span>
                                                  <span class="icon">★</span>
                                                  <span class="icon">★</span>
                                             </label>
                                             <label>
                                                  <input type="radio" name="stars" value="4" />
                                                  <span class="icon">★</span>
                                                  <span class="icon">★</span>
                                                  <span class="icon">★</span>
                                                  <span class="icon">★</span>
                                             </label>
                                             <label>
                                                  <input type="radio" name="stars" value="5" />
                                                  <span class="icon">★</span>
                                                  <span class="icon">★</span>
                                                  <span class="icon">★</span>
                                                  <span class="icon">★</span>
                                                  <span class="icon">★</span>
                                             </label>
                                        </form>

                                        <div class="row margin">
                                             <a href="{{route('laboratory-test',$laboratory->id)}}">
                                             <div class="col-md-12 btn_view text-center" style="background: #0080000d; font-size: 16px; font-weight: bold;">
                                                  DETAIL
                                             </div></a>
                                        </div>
                                   </div>
                              </div>
                              @endforeach
                              {{$all_laboratories->links()}}
                              </div>


          </div>
          <!--<all lab Data >-->

          <div class="tab-pane" id="tab_by_generic">
               <div class="container">
                    <div class="row">
                         <div class="col-md-12" style="padding-top: 20px;">
                              @foreach ($all_laboratories as $laboratory)
                              <div class="col-md-4">
                                   <div id="by_generic_detail_box">
                                        <div class="row">
                                             <div class="col-md-2">
                                                  <img src="{{env('CMS_URL').'/uploads/'.$laboratory->image}}" class="round_img">
                                             </div>
                                             <div class="col-md-4">
                                                  <h4 class="title"><a href="#" class="generic_title">{{$laboratory->name}}</a></h4>
                                             </div>
                                             <div class="col-md-4"> <a href="{{route('laboratory-test',$laboratory->id)}}"> <button type="button" class="btn btn-primary btn_viewmore">VIEW DETAIL<i class="fa fa-chevron-right" aria-hidden="true"></i></button></a></div>
                                        </div>
                                   </div>
                              </div>
                              @endforeach
                              {{$all_laboratories->links()}}

                         </div>


                    </div>
               </div>
          </div>



          <div class="tab-pane" id="search">
               <div class="col-md-12">
                    <section id="search_by_generic">
                         <div class="container">
                              <div class="row">
                                   @foreach ($all_laboratories as $laboratory)
                                   <div class="col-md-4">
                                        <div class="search_by_content_box">
                                             <h4 class="test_name">{{$laboratory->name}}</h4>
                                             <p>{{$laboratory->address}}</p>
                                             {{-- <p>{{$laboratory->description}}</p> --}}
                                            
                                             <span style="float: right;">
                                                  <a href="{{route('laboratory-test',$laboratory->id)}}"><button type="button" class="btn btn-primary btn_detail">DETAILS</button></a></span>

                                        </div>
                                   </div>
                                   @endforeach
                                   {{$all_laboratories->links()}}

                              </div>
                         </div>
                    </section>

               </div>

          </div>





     </div>
     <!--tab-content end>-->

     <!--row-->
     </div>
     <!--container-->
     </div>

</section>


<!-- Button trigger modal -->
{{-- <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#mobile_box">
     mobile Number Modal
</button> --}}
<!-- Modal -->
<section id="mobile_num_modal">
     <div class="modal fade" id="mobile_box" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
          aria-hidden="true">
          <div class="modal-dialog modal-dialog-centered" role="document">
               <div class="modal-content">

                    <h3 class="text-center">Big Health</h3>


                    <!--<div class="modal-body">-->
                    <!---->
                    <!--</div>-->
                    <div class="form__group field">
                         <input type="input" class="form__field" placeholder="Mobile Number" name="name" id='name'
                              required value="{{auth()->user()->phone ?? null}}" />

                    </div>
                    <div class="modal-footer">
                         <button type="button" class="btn btn-secondary btn_confirm"
                              data-dismiss="modal">Confirm</button>
                         &nbsp;
                         <button type="button" class="btn_cancel" data-dismiss="modal"
                              aria-label="Close">CANCEL</button>
                    </div>
               </div>
          </div>
     </div>

</section>
<!-- Button trigger modal -->
{{-- <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#inquire_form">
     inquire Form
</button> --}}
<section id="inquire_form_modal">
     <div class="modal fade" id="inquire_form" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
          aria-hidden="true">
          <div class="modal-dialog modal-dialog-centered" role="document">
               <div class="modal-content">

                    <h3 class="text-center">Add a Member</h3>


                    <!--<div class="modal-body">-->
                    <!---->
                    <!--</div>-->

                    <div class="form__group field">
                         <input type="input" class="form__field" placeholder="Full Name" name="name" id='name'
                              required />

                    </div>
                    <div class="form__group field">
                         <input type="input" class="form__field" placeholder="Last Name" name="name" id='name'
                              required />

                    </div>
                    <div class="form__group field">
                         <input type="input" class="form__field" placeholder="F/H Name" name="name" id='name'
                              required />

                    </div>
                    <div class="form__group field">
                         <select class="form-select form__field" aria-label="Default select example">
                              <option selected>Gender</option>
                              <option value="1">Male</option>
                              <option value="2">Female</option>

                         </select>

                    </div>
                    <div class="form__group field">
                         <input type="input" class="form__field" placeholder="Mobile Number" name="name" id='name'
                              required value="{{auth()->user()->phone ?? null}}"/>

                    </div>
                    <div class="form__group field">
                         <input type="input" class="form__field" placeholder="Date of Birth" name="name" id='name'
                              required />

                    </div>

                    <div class="modal-footer">
                         <button type="button" class="btn btn-secondary btn_confirm" data-dismiss="modal">ADD</button>
                         &nbsp;
                         <button type="button" class="btn_cancel" data-dismiss="modal"
                              aria-label="Close">CANCEL</button>
                    </div>
               </div>
          </div>
     </div>

</section>

<!-- Button trigger modal -->
{{-- <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#OTP_form">
     OPT-FORM
</button> --}}
<section id="OTP_form_modal">
     <div class="modal fade" id="OTP_form" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
          aria-hidden="true">
          <div class="modal-dialog modal-dialog-centered" role="document">
               <div class="modal-content">

                    <h3 class="text-center">Please Enter OTP</h3>



                    <div class="form__group field">
                         <input type="input" class="form__field" placeholder="OTP" name="name" id='name' required />

                    </div>




                    <div class="modal-footer">
                         <div class="col-md-10 btn_box">
                              <button type="button" class="btn btn-secondary btn_refresh" data-dismiss="modal">Resend
                                   OTP <i class="fa fa-refresh" aria-hidden="true"></i></button>
                              &nbsp;
                              <button type="button" class="btn_call" data-dismiss="modal" aria-label="Close">Call me <i
                                        class="fa fa-phone"></i></button>
                         </div>
                         <button type="button" class="btn btn-secondary btn_confirm"
                              data-dismiss="modal">CONFIRM</button>
                         &nbsp;
                         <button type="button" class="btn_cancel" data-dismiss="modal"
                              aria-label="Close">CANCEL</button>


                    </div>



               </div>
          </div>
     </div>

</section>


<!-- Button trigger modal -->
{{-- <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#family_form">
     Select_family-FORM
</button> --}}
<section id="select_Family_form_modal">
     <div class="modal fade" id="family_form" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
          aria-hidden="true">
          <div class="modal-dialog modal-dialog-centered" role="document">
               <div class="modal-content">

                    <h3 class="text-center">Blood C/E(Complete,CBC)</h3>


                    <p class="title_bar">Please Select Family Members</p>
                    <div class="row">

                         <div class="col-md-12 pd_switch">
                              <span class="span_name">Name 1</span>
                              <!-- Rounded switch -->
                              <label class="switch">

                                   <input type="checkbox">
                                   <span class="slider round"></span>
                              </label>

                         </div>
                         <div class="col-md-12 pd_switch">
                              <span class="span_name">Name 2</span>
                              <!-- Rounded switch -->
                              <label class="switch">
                                   <input type="checkbox">
                                   <span class="slider round"></span>
                              </label>
                         </div>

                    </div>


                    <div class="modal-footer">

                         <button type="button" class="btn btn-secondary btn_confirm"
                              data-dismiss="modal">CONFIRM</button>
                         &nbsp;
                         <button type="button" class="btn_cancel" data-dismiss="modal"
                              aria-label="Close">CANCEL</button>


                    </div>



               </div>
          </div>
     </div>

</section>


<!-- Button trigger modal -->
{{-- <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#Cart_SucessFully_form">
     Cart SucessFully updated
</button> --}}
<section id="Cart_SucessFully_form_modal">
     <div class="modal fade" id="Cart_SucessFully_form" tabindex="-1" role="dialog"
          aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
          <div class="modal-dialog modal-dialog-centered" role="document">
               <div class="modal-content">

                    <h3 class="text-center">LOGO</h3>


                    <p class="title_bar">Cart SuccessFully Updated</p>



                    <div class="modal-footer">

                         <!--<button type="button" class="btn btn-secondary btn_confirm" data-dismiss="modal">CONFIRM</button>-->
                         &nbsp;
                         <a href="" class="btn_confirm">Confirm</a>

                    </div>



               </div>
          </div>
     </div>

</section>
@endsection