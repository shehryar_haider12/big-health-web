@extends('layouts.master')
@section('custom-style')
@endsection
@section('content')


<!--&lt;!&ndash; Find Laboratory &ndash;&gt;-->
<section class="page-preloader">
     <div class="spinner">

          <span class="spinner-rotate"></span>
          
     </div>
</section>
<section id="home" class="slider" data-stellar-background-ratio="0.5">
     <div class="container">
          <div class="row">
               
               @php
                    $sliders = sliders();
               @endphp
               <div class="owl-carousel owl-theme">
                    <div class="item item-first" style="background-image: url({{url('').'/uploads/test.png'}})">
                         <div class="caption">
                              <div class="col-md-offset-1 col-md-10">
                                   {{-- <h3>{{$slider->heading}}</h3>
                                   <h1>{{$slider->text}}</h1> --}}
                                   <!--<a href="#team" class="section-btn btn btn-default smoothScroll">Meet Our Doctors</a>-->
                              </div>
                         </div>
                    </div>
                    {{-- @endforeach --}}
               </div>
          </div>
     </div>
</section>

<section id="lab_records">

     <div class="container">
          <div class="row">
               <!--<endbuttonsRow>-->

               <div class="tab-content">

                    <div class="tab-pane active" id="tab_All_lab">
                         <!--<all lab Data >-->
                              {{-- {{dd(env('CMS_URL'))}} --}}
                              <section id="search_by_generic">
                                   <div class="container">
                                        <div class="row">
                                             <table class="table table-bordered">
                                                  <thead>
                                                       <tr>
                                                            <td>S.No</td>
                                                            <td>Laboratory Name</td>
                                                            <td>Laboratory Address</td>
                                                            <td>Rate</td>
                                                            <td>Cart</td>
                                                       </tr>
                                                  </thead>
                                                  <tbody>
                                                       @php
                                                           $i=1;
                                                       @endphp
                                                       @foreach ($test_laboratories as $laboratory)
                                                       <tr>
                                                            <td>{{$i++}}</td>
                                                            <td>{{$laboratory->laboratory->name}}</td>
                                                            <td>{{$laboratory->laboratory->address}}</td>
                                                            <td>{{$laboratory->fee}} PKR</td>
                                                            <td>
                                                                 <button type="button" data-id="{{$laboratory->id}}" class="btn btn-primary cart mob_box"><i class="fa fa-shopping-cart"></i></button></span>
                                                            </td>
                                                       </tr>
                                                       @endforeach

                                                  </tbody>
                                             </table>
                                            
                                             {{$test_laboratories->links()}}
          
                                        </div>
                                   </div>
                              </section>


          </div>

     </div>
     <!--tab-content end>-->

     <!--row-->
     </div>
     <!--container-->
     </div>

</section>


<!-- Button trigger modal -->
{{-- <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#mobile_box">
     mobile Number Modal
</button> --}}
<!-- Modal -->
<section id="mobile_num_modal">
     <form action="#" id="send_opt">
          <div class="modal fade" id="mobile_box" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
          aria-hidden="true">
          <div class="modal-dialog modal-dialog-centered" role="document">
               <div class="modal-content">

                    <h3 class="text-center">Big Health</h3>
                    <!--<div class="modal-body">-->
                    <!---->
                    <!--</div>-->
                    <div class="form__group field">
                         <input type="input" class="form__field contact" placeholder="Mobile Number" name="name" id='contact' required value="{{auth()->user()->phone ?? null}}"/>
                         <input type="hidden" class="form__field test_id" name="test_id" id='test_id' required />

                    </div>
                    <div class="modal-footer">
                         <button type="submit" class="btn btn-secondary btn_confirm">Confirm</button>
                         &nbsp;
                         <button type="button" class="btn_cancel" data-dismiss="modal" aria-label="Close">CANCEL</button>
                    </div>
               </div>
          </div>
          </div>
     </form>
</section>
<!-- Button trigger modal -->
{{-- <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#inquire_form">
     inquire Form
</button> --}}
<section id="inquire_form_modal">
     <div class="modal fade" id="inquire_form" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
          aria-hidden="true">
          <div class="modal-dialog modal-dialog-centered" role="document">
               <div class="modal-content">

                    <h3 class="text-center">Add a Member</h3>


                    <!--<div class="modal-body">-->
                    <!---->
                    <!--</div>-->

                    <div class="form__group field">
                         <input type="input" class="form__field" placeholder="Full Name" name="name" id='name'
                              required />

                    </div>
                    <div class="form__group field">
                         <input type="input" class="form__field" placeholder="Last Name" name="name" id='name'
                              required />

                    </div>
                    <div class="form__group field">
                         <input type="input" class="form__field" placeholder="F/H Name" name="name" id='name'
                              required />

                    </div>
                    <div class="form__group field">
                         <select class="form-select form__field" aria-label="Default select example">
                              <option selected>Gender</option>
                              <option value="1">Male</option>
                              <option value="2">Female</option>

                         </select>

                    </div>
                    <div class="form__group field">
                         <input type="input" class="form__field" placeholder="Mobile Number" name="name" id='name'
                              required value="{{auth()->user()->phone ?? null}}"/>

                    </div>
                    <div class="form__group field">
                         <input type="input" class="form__field" placeholder="Date of Birth" name="name" id='name'
                              required />

                    </div>

                    <div class="modal-footer">
                         <button type="button" class="btn btn-secondary btn_confirm" data-dismiss="modal">ADD</button>
                         &nbsp;
                         <button type="button" class="btn_cancel" data-dismiss="modal"
                              aria-label="Close">CANCEL</button>
                    </div>
               </div>
          </div>
     </div>

</section>

<!-- Button trigger modal -->
{{-- <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#OTP_form">
     OPT-FORM
</button> --}}
<section id="OTP_form_modal">
     <form action="#" id="enter_opt">
     <div class="modal fade" id="OTP_form" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
          aria-hidden="true">
          <div class="modal-dialog modal-dialog-centered" role="document">
               <div class="modal-content">

                    <h3 class="text-center" >Please Enter OTP</h3>
                    <h5 class="text-center text-success" >You verification (OTP) code has been sent to your given phone number.</h5>
                    <span class="text-center text-success" id="opt_recie" ></span>
                    <div class="form__group field">
                         <input type="input" class="form__field otp" placeholder="OTP" name="otp" id='otp' required />

                    </div>
                    <div class="modal-footer">
                         <div class="col-md-10 btn_box">
                              <button type="button" id="resend" class="btn btn-secondary btn_refresh">Resend
                                   OTP <i class="fa fa-refresh" aria-hidden="true"></i></button>
                              &nbsp;
                              {{-- <button type="button" class="btn_call" data-dismiss="modal" aria-label="Close">Call me <i class="fa fa-phone"></i></button> --}}
                         </div>
                         <button type="submit" class="btn btn-secondary btn_confirm" >CONFIRM</button>
                         &nbsp;
                         <button type="button" class="btn_cancel" data-dismiss="modal"
                              aria-label="Close">CANCEL</button>


                    </div>



               </div>
          </div>
     </div>
     </form>
</section>


<!-- Button trigger modal -->
{{-- <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#family_form">
     Select_family-FORM
</button> --}}
<section id="select_Family_form_modal">
     <div class="modal fade" id="family_form" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
          aria-hidden="true">
          <div class="modal-dialog modal-dialog-centered" role="document">
               <div class="modal-content">

                    <h3 class="text-center">Blood C/E(Complete,CBC)</h3>


                    <p class="title_bar">Please Select Family Members</p>
                    <div class="row">

                         <div class="col-md-12 pd_switch">
                              <span class="span_name">Name 1</span>
                              <!-- Rounded switch -->
                              <label class="switch">

                                   <input type="checkbox">
                                   <span class="slider round"></span>
                              </label>

                         </div>
                         <div class="col-md-12 pd_switch">
                              <span class="span_name">Name 2</span>
                              <!-- Rounded switch -->
                              <label class="switch">
                                   <input type="checkbox">
                                   <span class="slider round"></span>
                              </label>
                         </div>

                    </div>


                    <div class="modal-footer">

                         <button type="button" class="btn btn-secondary btn_confirm"
                              data-dismiss="modal">CONFIRM</button>
                         &nbsp;
                         <button type="button" class="btn_cancel" data-dismiss="modal"
                              aria-label="Close">CANCEL</button>


                    </div>



               </div>
          </div>
     </div>

</section>


<!-- Button trigger modal -->
{{-- <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#Cart_SucessFully_form">
     Cart SucessFully updated
</button> --}}
<section id="Cart_SucessFully_form_modal">
     <div class="modal fade" id="Cart_SucessFully_form" tabindex="-1" role="dialog"
          aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
          <div class="modal-dialog modal-dialog-centered" role="document">
               <div class="modal-content">

                    <h3 class="text-center">LOGO</h3>


                    <p class="title_bar">Cart SuccessFully Updated</p>



                    <div class="modal-footer">

                         <!--<button type="button" class="btn btn-secondary btn_confirm" data-dismiss="modal">CONFIRM</button>-->
                         &nbsp;
                         <a href="{{route('cart')}}" class="btn_confirm">Confirm</a>

                    </div>



               </div>
          </div>
     </div>

</section>
@section('custom-script')
<script>
     $(document).ready(function () {
          $('.mob_box').click(function(){
               var test_id = $(this).data('id');
               $('.test_id').val(test_id);
               $('#mobile_box').modal({
                    show: 'true'
               }); 
              
          });
          $('#send_opt').submit(function (e) {
               e.preventDefault();
               $(window).load(function(){
                    $('.page-preloader').fadeIn(); // set duration in brackets    
               });
               var $this      = $(this);
               var contact    = $('.contact').val();
               var id         = 1;

               axios
               .post('{{route("generate.otp")}}', {
               _token: '{{csrf_token()}}',
               _method: 'post',
               id: id,
               contact: contact,
               })
               .then(function (responsive) {

                    $('#mobile_box').removeClass('in');
                    $("#mobile_box").css("display", "none")
                    $('#OTP_form').modal({
                         show: 'true'
                    }); 
               // $('#OTP_form').addClass('in');
               $('#opt_recie').html(responsive.data.data.code);
               console.log(responsive.data);
               })
               .catch(function (error) {
               console.log(error);
               });
          });
          $('#enter_opt').submit(function (e) {
               e.preventDefault();
               $(window).load(function(){
                    $('.page-preloader').fadeIn(); // set duration in brackets    
               });
               var $this      = $(this);
               var code       = $('.otp').val();
               var id         = 1;
               var test_id    = $('.test_id').val();
               console.log(test_id);
               axios
               .post('{{route("verify.otp")}}', {
               _token: '{{csrf_token()}}',
               _method: 'post',
               id: id,
               code: code,
               test_id: test_id,
               })
               .then(function (responsive) {

               // $('#OTP_form').addClass('in');
               $('#opt_recie').html(responsive.data.data.code);
               $('.mob_box').css('background','#4dcc71 !important');
               $('.mob_box').css('color','white !important');
               $('#opt_recie').modal({
                    show: 'false'
               }); 
               $('#Cart_SucessFully_form').modal({
                    show: 'true'
               }); 

               console.log(responsive.data);
               })
               .catch(function (error) {
               console.log(error.response.data.errors);
                    swal(
                    'Failed!',
                    error.response.data.errors,
                    'error'
                    )
               });
          });
          $('#resend').click(function () {
               var contact    = $('.contact').val();
               var id         = 1;
               console.log(contact);
               axios
               .post('{{route("generate.otp")}}', {
               _token: '{{csrf_token()}}',
               _method: 'post',
               id: id,
               contact: contact,
               })
               .then(function (responsive) {

               swal(
                    'success!',
                    'Resend Successfully',
                    'success'
                    )
               // $('#OTP_form').addClass('in');
               $('#opt_recie').html(responsive.data.data.code);
               console.log(responsive.data);
               })
               .catch(function (error) {
               console.log(error);
               });
          });
          
     });
</script>
@endsection
@endsection