@extends('layouts.master')
@section('content')
    <!--&lt;!&ndash; HOME &ndash;&gt;-->
<section id="home" class="slider" data-stellar-background-ratio="0.5">
     <div class="container">
          <div class="row">
               @php
                   $sliders = sliders();
               @endphp
               <div class="owl-carousel owl-theme">
                    @foreach ($sliders as $slider)
                    <div class="item item-first" style="background-image: url({{env('CMS_URL').'/uploads/'}}{{$slider->image ?? 'placeholder.jpg'}})">
                         <div class="caption">
                              <div class="col-md-offset-1 col-md-10">
                                   <h3>{{$slider->heading}}</h3>
                                   <h1>{{$slider->text}}</h1>
                                   <!--<a href="#team" class="section-btn btn btn-default smoothScroll">Meet Our Doctors</a>-->
                              </div>
                         </div>
                    </div>
                    @endforeach
               </div>
          </div>
     </div>
</section>

<section id="feature_ser">
     <div class="container" style="margin:0 auto ">
          <div class="row">
               <div class="col-md-12">
                    <h1>Featured Services</h1>
                    <div class="row">
                         @foreach ($featured_services as $service)
                              {{-- <a href="{{route('category-test',$service->id)}}"> --}}
                              <a href="#">
                                   <div class="col-md-3">
                                        <div class="box">
                                             <img class="size" src="{{env('CMS_URL')}}/uploads/{{$service->image ?? 'placeholder.jpg'}}" style="height: 200px;">
                                             <div class="centered ">{{$service->name}} </div>
                                        </div>
                                   </div>
                              </a>
                         @endforeach

                         {{-- <div class="col-md-3">
                              <div class="box">
                                   <img class="size" src="{{url('')}}/assets/images/news-image.jpg">
                                   <div class="centered">Centered</div>
                              </div>
                         </div>
                         <div class="col-md-3">
                              <div class="box">
                                   <img class="size" src="{{url('')}}/assets/images/news-image.jpg">
                                   <div class="centered">Centered</div>
                              </div>
                         </div>

                         <div class="col-md-3">
                              <div class="box">

                                   <img class="size" src="{{url('')}}/assets/images/news-image.jpg">
                                   <div class="centered">Centered</div>
                              </div>
                         </div>
                         <div class="col-md-3">
                              <div class="box">

                                   <img class="size" src="{{url('')}}/assets/images/news-image.jpg">
                                   <div class="centered">Centered</div>
                              </div>
                         </div> --}}
                    </div>
               </div>
          </div>
     </div>
     <div class="sprater"></div>
</section>

<section id="quality_offer">
     <div class="container">
          <div class="row">
               <div class="col-md-12">
                    <h3>We are offering <span>Quality Services</span></h3>

                    <p>Chughtai Lab has always focused on quality in order to earn the trust of patients and doctors.The
                         lab is staffed by 20 pathologists and more than 70 BSc/MSc/MPhil lab technologists. Chughtai
                         Lab is ISO 15189 certified and also participates in the College of American Pathologists
                         external Quality Assurance program.</p>
                    <div class="row" style="    padding-top: 44px;
    background: white;
    padding-bottom: 44px;
    border-radius: 11px;">


                         <div class="col-md-4">

                              <div class="card">
                                   <article class="card-group-item">
                                        <div class="filter-content">
                                             <div class="list-group list-group-flush">
                                                  <a href="#tab_a" data-toggle="pill" class="list-group-item">Free Home
                                                       Sample Collection </a>
                                                  <a href="#tab_b" data-toggle="pill" class="list-group-item">Chughtai
                                                       Medical Center </a>
                                                  <a href="#tab_c" data-toggle="pill" class="list-group-item">Radiology
                                                       Services </a>
                                                  <a href="#tab_d" data-toggle="pill" class="list-group-item">Chughtai
                                                       Lab Homecare </a>
                                                  <a href="#tab_e" data-toggle="pill" class="list-group-item">Chughtai
                                                       Pharmacy </a>

                                             </div> <!-- list-group .// -->
                                        </div>
                                   </article> <!-- card-group-item.// -->

                              </div> <!-- card.// -->


                         </div>

                         <div class="tab-content">
                              <div class="tab-pane active" id="tab_a">
                                   <div class="col-md-4">

                                        <img src="{{url('')}}/assets/images/news-image.jpg" />

                                   </div>

                                   <div class="col-md-4">

                                        <h4 class="title">Free Home Sample Collection</h4>
                                        <p>Chughtai Lab is offering free home sample collection services to the patients
                                             to help them get tested in the convenience of their homes. To book home
                                             sample collection call 03-111 456 789.</p>

                                        <button type="button" class="btnreadmore btn btn-primary ">Read More</button>
                                   </div>

                              </div>

                              <div class="tab-pane " id="tab_b">
                                   <div class="col-md-4">

                                        <img src="{{url('')}}/assets/images/news-image.jpg" />
                                   </div>

                                   <div class="col-md-4">

                                        <h4 class="title">Radiology Services</h4>
                                        <p>Providing a comprehensive diagnostic facility under one roof. Our radiology
                                             services are an attempt to deliver top quality radiology services to our
                                             patients, with the same level of convenience and reliability that Chughtai
                                             Lab is known for.…</p>

                                        <button type="button" class="btnreadmore btn btn-primary ">Read More</button>
                                   </div>

                              </div>


                         </div>



                    </div>




               </div>
          </div>
     </div>

</section>

<section id="into_sec">
     <div class="container">
          <div class="row">
               <div class="col-md-12">
                    <h1>What is big health?</h1>
                    <div class="row">
                         <i class="fa fa-quote-left"></i>
                         <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has
                              been the industry's standard dummy text ever since the 1500s, when an unknown printer took
                              a galley of type and scrambled it to make a type specimen book. It has survived not only
                              five centuries, but also the leap into electronic typesetting, remaining essentially
                              unchanged. It was popularised in the 1960s with the release of Letraset sheets containing
                              Lorem Ipsum passages, and more recently with desktop publishing software like Aldus
                              PageMaker including versions of Lorem Ipsum.</p>
                         <i class="fa fa-quote-right"></i>
                    </div>


               </div>
          </div>
     </div>
</section>

@endsection