@extends('layouts.master')
@section('content')

<!--&lt;!&ndash; HOME &ndash;&gt;-->
<section id="home" class="slider" data-stellar-background-ratio="0.5">
    <div class="container">
        <div class="row">
            @php
                $sliders = sliders();
                if($banners->count() > 0)
                {
                    // return "dfdfa";
                    $sliders = $banners;
                }
            @endphp
            <div class="owl-carousel owl-theme">
                @foreach ($sliders as $slider)
                    <div class="item item-first" style="background-image: url({{env('CMS_URL').'/uploads/'}}{{$banners->count() > 0 ? $slider->banner : $slider->image ?? 'placeholder.jpg'}})">
                        <div class="caption">
                            <div class="col-md-offset-1 col-md-10">
                                    {{-- <h3>{{$slider->heading}}</h3>
                                    <h1>{{$slider->text}}</h1> --}}
                                    <!--<a href="#team" class="section-btn btn btn-default smoothScroll">Meet Our Doctors</a>-->
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>

        </div>
    </div>
</section>


<section id="Test_detail">
   <div class="container">
       <div class="row">
           <div class="col-md-5">
               <div class="box_info">
                   <h6 class="title_test">  {{$test_detail->test->name}}</h6>
                   <h6 class="description">  {{$test_detail->test->recommend}}</h6>
                   <p class="description">{{$test_detail->test->description}}</p>
                   <hr>
                   <button type="button" class="btn btn-secondary price_title" data-dismiss="modal">{{$test_detail->fee}} PKR</button>
               </div>
               <div class="box_info">
                   <h6 class="title_test">  {{$test_detail->Laboratory->name}}</h6>
                   <p class="description">{{$test_detail->laboratory->address}}</p>
                   <p class="description">{{$test_detail->laboratory->description}}</p>
               </div>
            </div>
            <div class="col-md-6 col-md-offset-1">
             <div class="justification_info">
               <h4 class="title_justi">Includes Test - {{$test_detail->includeTests->count()}} </h4>
                    @foreach ($test_detail->includeTests as $test)
                        <p class="des_justi">{{$test->test->name}}</p>
                    @endforeach 
             </div>
           </div>
      </div>
   </div>
</section>
@endsection